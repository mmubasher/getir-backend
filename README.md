# Getir Backend Assignment

Simple API to fetch records by a filter

# Configure Project

## Documentation 
```bash
/documentation
```
## Installation
Install dependencies

```bash
npm install
```
## Set environment variables 

```bash
cp .env.example .env
```

Clone **.env.example** with name **.env** and replace the default values with actual values


## Run
Start the app

```bash
npm start
```


## Testing
Run the following command on your terminal:

```bash
npm test

# Run test along with code coverage
yarn test:coverage
```

# Available endpoints:

## Records
| Path                   | Method | Description                            |
| ---------------------- | ------ | -------------------------------------- |
| /api/records           | GET    | Get records                

## Misc
| Path              | Method | Description                    |
| ----------------- | ------ | ------------------------------ |
| /api/health-check | GET    | API/Container health check API |

