const express = require('express');
const recordRoutes = require('./src/record/record.route');

const router = express.Router();

/** GET / - Check service health */
router.get(['/', '/health-check'], (req, res) => res.send('OK'));

// mount user routes at /records
router.use('/records', recordRoutes);

module.exports = router;
