const Record = require('./record.model');

/**
 * Get record list.
 * @returns {Record[]}
 */
function filter(req, res, next) {
  Record.filter(req.body)
    .then((records) => res.json({
      statusCode: 200,
      message: 'Success',
      records
    }))
    .catch((e) => next(res.boom.internal(e)));
}

module.exports = {
  filter
};
