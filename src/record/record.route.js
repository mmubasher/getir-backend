const express = require('express');
const { validate, Joi } = require('express-validation');

const router = express.Router();
const recordCtrl = require('./record.controller');

// Request Validation Schema
const filterRecords = {
  body: Joi.object({
    startDate: Joi.date().required(),
    endDate: Joi.date().required().min(Joi.ref('startDate')),
    minCount: Joi.number().required(),
    maxCount: Joi.number().required().min(Joi.ref('minCount'))
  })
};

router.route('/')
  /** GET /api/records - Get filtered list of records */
  .post(validate(filterRecords, { keyByField: true }, {}), recordCtrl.filter);

module.exports = router;
