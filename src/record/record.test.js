const request = require('supertest');
const httpStatus = require('http-status');
const chai = require('chai');

const { expect } = chai;
const app = require('../../app');
const testHelper = require('../helpers/test-helper');

chai.config.includeStack = true;
/**
 * root level hooks
 */
before(async () => {
  await testHelper.startDatabase();
});
after(async () => {
  await testHelper.clearDatabase();
  await testHelper.closeDatabase();
  await testHelper.stopDatabase();
});

describe('## Record APIs', () => {
  describe('# GET /api/records/', () => {
    it('should get filtered records', (done) => {
      request(app)
        .post('/api/records')
        .send({
          startDate: (new Date()).toISOString(),
          endDate: (new Date()).toISOString(),
          minCount: 200,
          maxCount: 1000
        })
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.body.records).to.be.an('array');
          done();
        })
        .catch(done);
    });
  });

  describe('# Error Handling', () => {
    it('should handle express validation error - maxCount is required', (done) => {
      request(app)
        .post('/api/records')
        .send({
          startDate: '2016-01-26',
          endDate: '2018-02-02',
          minCount: 200
        })
        .expect(httpStatus.BAD_REQUEST)
        .then((res) => {
          expect(res.body.message).to.equal('"maxCount" is required');
          done();
        })
        .catch(done);
    });

    it('should handle express validation error - invalid startDate date', (done) => {
      request(app)
        .post('/api/records')
        .send({
          startDate: 'xyzInvalid',
          endDate: '2018-02-02',
          minCount: 200,
          maxCount: 200,
        })
        .expect(httpStatus.BAD_REQUEST)
        .then((res) => {
          expect(res.body.message).to.equal('"startDate" must be a valid date');
          done();
        })
        .catch(done);
    });
  });
});
