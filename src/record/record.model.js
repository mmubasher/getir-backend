const mongoose = require('mongoose');
/**
 * Record Schema
 */
const RecordSchema = new mongoose.Schema({
  createdAt: {
    type: Date,
    required: true
  },
  key: {
    type: String,
    required: true
  },
  counts: {
    type: Array,
    required: true
  },
});
/**
 * Statics
 */
RecordSchema.statics = {
  /**
   * List records
   * @returns {Promise<Record []>}
   */
  filter({
    startDate,
    endDate,
    minCount,
    maxCount
  }) {
    return this.aggregate(
      [
        {
          $match: {
            $and: [
              {
                createdAt: {
                  $gte: new Date(startDate)
                }
              },
              {
                createdAt: {
                  $lte: new Date(endDate)
                }
              }
            ]
          }
        },
        {
          $project: {
            _id: false,
            key: 1,
            createdAt: 1,
            totalCount: {
              $sum: '$counts'
            }
          }
        },
        {
          $match: {
            $and: [
              {
                totalCount: {
                  $gte: minCount
                }
              },
              {
                totalCount: {
                  $lte: maxCount
                }
              }
            ]
          }
        },
        {
          $sort: {
            createdAt: -1 // sort on desc order
          }
        }
      ]
    )
      .exec();
  }
};

/**
 * @typedef Record
 */
module.exports = mongoose.model('Record', RecordSchema);
