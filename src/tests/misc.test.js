const request = require('supertest');
const httpStatus = require('http-status');
const chai = require('chai'); // eslint-disable-line import/newline-after-import
const { expect } = chai;
const app = require('../../config/express');

chai.config.includeStack = true;

describe('## Misc', () => {
  describe('# GET /', () => {
    it('should return 200 HTTP Status Code', (done) => {
      request(app)
        .get('/')
        .expect(httpStatus.OK)
        .then(() => {
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /api/health-check', () => {
    it('should return OK', (done) => {
      request(app)
        .get('/api/health-check')
        .expect(httpStatus.OK)
        .then((res) => {
          expect(res.text).to.equal('OK');
          done();
        })
        .catch(done);
    });
  });

  describe('# GET /random-url', () => {
    it('should return 404 status', (done) => {
      request(app)
        .get('/random-url')
        .expect(httpStatus.NOT_FOUND)
        .then((res) => {
          expect(res.body.message).to.equal('API not found');
          done();
        })
        .catch(done);
    });
  });
});
