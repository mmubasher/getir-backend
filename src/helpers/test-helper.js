const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');

let mongod = null;

module.exports.startDatabase = async () => {
  mongod = new MongoMemoryServer();
  const mongoUri = await mongod.getConnectionString();
  mongoose.connect(mongoUri, {
    useNewUrlParser: true, useUnifiedTopology: true, keepAlive: true, useCreateIndex: true,
  });
  mongoose.connection.on('error', () => {
    throw new Error(`unable to connect to database: ${mongoUri}`);
  });
};

/**
 * Drop database, close the connection and stop mongod.
 */
module.exports.closeDatabase = async () => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
};

/**
 * Drop database, close the connection and stop mongod.
 */
module.exports.stopDatabase = async () => {
  if (mongod) {
    await mongod.stop();
  }
};

/**
 * Remove all the data for all db collections.
 */
module.exports.clearDatabase = async () => {
  const { collections } = mongoose.connection;

  Object.keys(collections).forEach(async (key) => {
    const collection = collections[key];
    await collection.deleteMany();
  });
};
