const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const { ValidationError } = require('express-validation');
const expressBoom = require('express-boom');
const Boom = require('boom');
const SwaggerUi = require('swagger-ui-express');
const httpStatus = require('http-status');
const routes = require('../app.route');
const config = require('./config');
const SwaggerDoc = require('../swagger.json');

const app = express();

if (config.env === 'development') {
  app.use(logger('dev'));
}

// parse body params and attache them to req.body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// Fancy Errors
app.use(expressBoom());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

app.get('/', (req, res) => res.send(config.env.projectTitle));
// mount all routes on /api path
app.use('/api', routes);
// Add Documentation
app.use('/documentation', SwaggerUi.serve, SwaggerUi.setup(SwaggerDoc));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  res.boom.notFound('API not found');
  return next();
});

// error handler, send stacktrace only during development
app.use((err, req, res, next) => {
  if (err instanceof ValidationError) {
    const unifiedErrorMessage = err.details.map((error) => Object.values(error)
      .join(''))
      .join(' and ');
    res.boom.badRequest(unifiedErrorMessage);
  } else {
    Boom.boomify(new Error(err), err.status ? err.status : httpStatus.INTERNAL_SERVER_ERROR,
      config.env === 'development' ? err.stack : null);
  }
  return next();
});

module.exports = app;
